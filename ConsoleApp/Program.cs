﻿// See https://aka.ms/new-console-template for more information
namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var random = new Random();
            int essais = 5;
            Console.WriteLine("Valeur entre 0 et 100 !");
            int rand = random.Next(101);
            for(int i = 0; i < essais; i ++){
                int valeur = int.Parse(Console.ReadLine());
                if(valeur == rand){
                    Console.WriteLine($"{Environment.NewLine} Trouvé ! La valeur était {valeur}");
                    break;
                } else {
                    Console.WriteLine($"{Environment.NewLine} " + (valeur < rand ? $"{valeur} < Cible" : $"{valeur} > Cible ") + $" Tentative : {i}.");
                }
                if(i == 4) Console.WriteLine($"perdu : {rand}");
            }
            Console.WriteLine($"{Environment.NewLine} press key to exit");
            Console.ReadKey(true);
        }
    }
}