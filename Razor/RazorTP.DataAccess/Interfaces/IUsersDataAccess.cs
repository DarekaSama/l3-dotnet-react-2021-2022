﻿using RazorTP.Common.Models;

namespace RazorTP.DataAccess.Interfaces {
    public interface IUsersDataAccess {
        Task<IEnumerable<User>> GetAllUsers();
        Task<User?> GetById(int id);
        Task AddUser(User? user);
    }
}
