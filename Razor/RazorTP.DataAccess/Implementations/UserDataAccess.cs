﻿using RazorTP.Common.Models;
using RazorTP.DataAccess.Interfaces;

namespace RazorTP.DataAccess.Implementations {
    public class UserDataAccess : IUsersDataAccess {
        private readonly List<User> usersDb = new();

        public UserDataAccess() {
            usersDb.Add(new User {
                Id = 1,
                Name = "A",
                Age = 12
            });

            usersDb.Add(new User {
                Id = 2,
                Name = "B",
                Age = 25
            });

            usersDb.Add(new User {
                Id = 3,
                Name = "C",
                Age = 60
            });
        }

        public async Task<IEnumerable<User>> GetAllUsers() {
            return usersDb;
        }
        
        public async Task<User?> GetById(int id) {
            return usersDb?.FirstOrDefault(x => x.Id == id);
        }
        
        public async Task AddUser(User? user) {
            usersDb?.Add(user);
        }
    }
}
