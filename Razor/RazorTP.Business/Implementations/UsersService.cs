﻿using RazorTP.Business.Interfaces;
using RazorTP.Common.Models;
using RazorTP.DataAccess.Implementations;
using RazorTP.DataAccess.Interfaces;

namespace RazorTP.Business.Implementations {
    public class UsersService : IUsersService {
        private readonly IUsersDataAccess usersDataAccess;

        public UsersService(IUsersDataAccess usersDataAccess) {
            this.usersDataAccess = usersDataAccess;
        }

        public async Task<IEnumerable<User>> GetAllUsers() {
            return await usersDataAccess.GetAllUsers();
        }

        public async Task<double> GetAverageAge()
        {
            return (await usersDataAccess.GetAllUsers()).Average(x => x.Age);
        }
        
        public async Task<User?> GetById(int id)
        {
            return await usersDataAccess.GetById(id);
        }

        public async Task AddUser(User? user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));
            if (!string.IsNullOrWhiteSpace(user.Name)) throw new ArgumentException(nameof(user.Name));
            await usersDataAccess.AddUser(user);
        }
    }
}
