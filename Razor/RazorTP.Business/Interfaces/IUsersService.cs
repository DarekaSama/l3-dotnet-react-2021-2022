﻿using RazorTP.Common.Models;

namespace RazorTP.Business.Interfaces {
    public interface IUsersService {
        Task<IEnumerable<User>> GetAllUsers();
        Task<double> GetAverageAge();
        Task<User?> GetById(int id);
        Task AddUser(User? user);
    }
}
