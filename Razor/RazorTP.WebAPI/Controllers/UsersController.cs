﻿using Microsoft.AspNetCore.Mvc;
using RazorTP.Business.Interfaces;
using RazorTP.Common.Models;

namespace RazorTP.WebAPI.Controllers {
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase {
        private readonly IUsersService usersService;

        public UsersController(IUsersService usersService) {
            this.usersService = usersService;
        }

        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<User>>> GetAll() {
            return Ok(await usersService.GetAllUsers());
        }

        [HttpGet("averageAge")]
        public async Task<ActionResult<double>> GetAverageAge()
        {
            return Ok(await usersService.GetAverageAge());
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<User?>> GetById(int id)
        {
            var user = await usersService.GetById(id);
            if (user == default) return NoContent();
            return Ok(user);
        }
        
        [HttpPost("add")]
        public async Task<ActionResult> AddUser([FromBody] User? user) {
            try {
                await usersService.AddUser(user);

                return Ok();
            } catch (Exception e) {
                return BadRequest();
            }
        }
    }
}
