using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace RazorTP.WebAPI.HealthCheck {
    public class DbHealthCheck : IHealthCheck {
        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default) {
            var random = new Random();
            var isHealthy = random.Next(3) > 1;

            if (isHealthy) {
                return Task.FromResult(
                    HealthCheckResult.Healthy("A healthy result.")
                );
            }

            return Task.FromResult(
                new HealthCheckResult(
                    context.Registration.FailureStatus,
                    "An unhealthy result."
                )
            );
        }
    }
}