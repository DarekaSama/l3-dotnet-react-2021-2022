import './index.css';
export default function MyComponent({name}) {
    return (
        <span className="my-component">
            My Component parce que c'est genial. 
            <p>Hello {name}</p>
        </span>
    );
}