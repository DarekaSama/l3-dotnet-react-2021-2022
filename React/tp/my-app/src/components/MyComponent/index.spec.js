import { render, screen } from '@testing-library/react';
import MyComponent from '../MyComponent';

test('renders learn react link', () => {
    render(<MyComponent name={'world'} />);
    const linkElement = screen.getByText(/Hello world/i);
    expect(linkElement).toBeInTheDocument();
});
